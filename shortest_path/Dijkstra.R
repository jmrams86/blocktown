############################################################
#### Dijkstra shortest path algorithm between two nodes ####
############################################################

library(tidyverse)

# Setup values
n_nodes <- 99
map_size <- 15
network_density_factor <- 1.0 # lowest density at 0.5, higher numbers = higher density of connections

# Create node table
x <- round(runif(n_nodes, 1, map_size),0)  # X-axis position
y <- round(runif(n_nodes, 1, map_size),0)  # Y-axis position
nodes <- data.frame('node'= c(1:n_nodes), 'x'= x, 'y'= y)

# Create adjacency matrix (i.e. links between the nodes) alows only single direction links (like transport links can be)
adj_mat <- matrix(round(runif(n_nodes^2, 0, network_density_factor),0), nrow = n_nodes, ncol = n_nodes)
adj_mat[adj_mat<1] = 0
adj_mat[adj_mat>=1] = 1
adj_mat

# (OPTIONAL) Mirror so two way
for (i in 1:n_nodes){
  for (j in 1:n_nodes){
    if (adj_mat[i,j] == 1){
      adj_mat[j,i] <- 1
    }
  }
}

# Function for straight line (Pythagoras) distance bw nodes 
getdistance <- function(x1, y1, x2, y2){
  dist <- sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
  return (dist)
}

# Plot network
net <- ggplot(nodes, aes(x, y)) + 
  geom_point() +
  xlim(0, map_size) +
  ylim(0, map_size)

for (i in (1:n_nodes)){
  for (j in (1:n_nodes)){
    if ((adj_mat[i,j] < 1 | i == j)){
      next
    }
    dist_label_x_pos <- (max(nodes$x[i],nodes$x[j])+min(nodes$x[i],nodes$x[j]))/2
    dist_label_y_pos <- (max(nodes$y[i],nodes$y[j])+min(nodes$y[i],nodes$y[j]))/2
    label_text <- round(getdistance(nodes$x[i],nodes$y[i],nodes$x[j],nodes$y[j]),2)
    line <- annotate('segment', x = nodes$x[i], xend = nodes$x[j], y = nodes$y[i], yend = nodes$y[j], colour = 'blue')
    dist <- annotate('label', x = dist_label_x_pos, y= dist_label_y_pos, label = label_text, colour = "red", size = 3)
    net <- net + line + dist
  }
  net <- net + annotate('label', x = x, y = y, label = nodes$node)
}


getShortestPaths <- function(start_node){
  # Define starting node for path search origin
  current_node <- start_node
  
  # Storage for results
  distances <- data.frame("i" = start_node,
                          "j" = 1:n_nodes,
                          'dist' = Inf)
  distances$dist <- ifelse(distances$j == start_node, 0, Inf)
  
  visited <- c() # visited vertices
  
  # While there are unvisited nodes run through updating paths based on all links (edges)
  while (length(visited) != n_nodes){
    
    visited <- unique(c(visited, current_node)) # update visited list
    unvisited <- (!1:n_nodes %in% visited)*1:n_nodes # update unvisited list
    
    edges <- adj_mat[current_node,]*1:n_nodes # get edge number that connect to the current node
    edges <- edges[edges>0]
    
    # For each of these edges check if they are the shortest, if they are update final distances, else move to next
    for (i in edges){
      current_distance <- distances$dist[i]
      edge_distance <- getdistance(nodes$x[current_node],nodes$y[current_node],nodes$x[i],nodes$y[i])
      new_distance <- distances$dist[current_node] + edge_distance
      
      if (new_distance < current_distance){
        distances$dist[i] <- new_distance # update distance for that node with newly found shorter route
      }
    }
    current_node <- min(unvisited[unvisited>0], Inf) # Move to next node
  }
  return(distances)
}

# Long matrix
results <- lapply(1:n_nodes, getShortestPaths) %>% 
  bind_rows

# Wide matrix
results_wide <- spread(results, j, dist)

